import React, { useState, useEffect } from "react";
import { LinearGradient } from "expo-linear-gradient";
import styles from "./style";
import { Dimensions, Image, View, Linking, Platform } from "react-native";
import { Button, Text } from "galio-framework";
import * as TaskManager from "expo-task-manager";
import * as Location from "expo-location";
import * as IntentLauncher from "expo-intent-launcher";
import "firebase/firestore";
import firebase from "../../utility/Firebase";
import * as Notifications from "expo-notifications";
import logoApp from "../../assets/logoApp.png";

//Notifications
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const dbh = firebase.firestore();

const { width, height } = Dimensions.get("screen");
export default function TermsScreen({ navigation }) {
  //************************* */
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [modalVisibal, setModalVisibal] = useState(false);
  const [openSettings, setOpenSettings] = useState(false);
  async function enterNotification() {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "❌❌ be careful ❌❌",
        body: "Il'ya qq1 porteur du COVID-19 proche de vous",
      },
      trigger: { seconds: 2 },
    });
  }
  async function leaveNotification() {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "✅✅ You are safe now ✅✅",
        body: "You are out of the infected zone ✅",
      },
      trigger: { seconds: 2 },
    });
  }
  // const regionMalade = [];
  const [regionMalade, setRegionMalade] = useState([]);
  let text = "Waiting..";
  let X = "";
  let Y = "";
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
    X = JSON.stringify(location.coords.latitude);
    Y = JSON.stringify(location.coords.longitude);
  }
  TaskManager.defineTask(
    "REGIONS_MALADE",
    ({ data: { eventType, region }, error }) => {
      // console.warn("aaaa");
      if (error) {
        // console.warn(error.message);
        return;
      }
      if (eventType === Location.GeofencingEventType.Enter) {
        // console.warn("You've entered region:", region);
        enterNotification();
      } else if (eventType === Location.GeofencingEventType.Exit) {
        // console.warn("You've left region:", region);
        leaveNotification();
      }
    }
  );

  useEffect(() => {
    (async () => {
      try {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== "granted") {
          setErrorMsg("Permission to access location was denied");
          return;
        }

        let location = await Location.getCurrentPositionAsync({
          enableHighAccuracy: true,
        });
        setLocation(location);
      } catch (error) {
        let status = Location.getProviderStatusAsync();
        if (!status.locationServicesEnabled) {
          setModalVisibal(true);
        }
      }
    })();
  }, []);

  useEffect(() => {
    dbh.collection("RegionMalade").onSnapshot((documentSnapshot) => {
      let data = [];
      let i = 1;
      documentSnapshot.forEach((doc) => {
        data.push({
          identifier: i.toString(),
          latitude: parseFloat(doc.data().location[0]),
          longitude: parseFloat(doc.data().location[1]),
          radius: 70,
          notifyOnEnter: true,
          notifyOnExit: true,
        });
        i++;
      });
      setRegionMalade(data);
      // console.warn("regionMalade: ", data);
    });
  }, []);
  const scanRegionHandler = () => {
    if (regionMalade.length) {
      // console.warn("regionMalade 2: ", regionMalade);
      Location.startGeofencingAsync("REGIONS_MALADE", regionMalade)
        .then((response) => {
          // console.warn("bbb");
          // console.warn("resp: ", response);
          if (response) {
            // console.warn("Response from startGeoFencingAsync() = " + response);
          }
        })
        .catch((error) => {
          if (error) {
            // console.warn("Error from startGeoFencingAsync() = " + error);
          }
        });
    }
  };

  const settings = () => {
    if (Platform.OS == "ios") {
      Linking.openURL("app-settings:");
    } else {
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS
      );
    }
    setOpenSettings(false);
  };

  return (
    <View>
      <LinearGradient
        // Background Linear Gradient
        colors={["white", "#4096e9"]}
        style={{
          width: width,
          height: height,
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
        }}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1.3 }}
      />
      <View style={{ alignItems: "center", height: height * 0.25 }}>
        <Image
          source={logoApp}
          style={{
            width: width * 0.9,
            height: 250,
          }}
        />
      </View>
      <View style={{ alignItems: "center", height: height * 0.55 }}>
        <Text style={{ fontSize: 16, marginBottom: 30 }}>
          J'ai lu et j'accepte les termes d'utilisation
        </Text>
      </View>
      <View
        style={{
          alignItems: "center",
          height: height * 0.2,
          justifyContent: "center",
        }}
      >
        <Button
          color="#3f97e9"
          style={{ width: "80%", borderRadius: 50 }}
          onPress={() => scanRegionHandler()}
        >
          Scan envirement !!!
        </Button>
        <Button
          color="#3f97e9"
          style={{ width: "80%", borderRadius: 50 }}
          onPress={() => navigation.navigate("Login")}
        >
          J'accepte
        </Button>
      </View>
    </View>
  );
}
