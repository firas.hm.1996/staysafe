import React, { useState, useEffect, useRef } from "react";
import {
  Platform,
  View,
  Image,
  Dimensions,
  Linking,
  TouchableOpacity,
} from "react-native";
import * as TaskManager from "expo-task-manager";
import { Button, Text } from "galio-framework";
import * as Location from "expo-location";
import * as IntentLauncher from "expo-intent-launcher";
import styles from "./style";
import "firebase/firestore";
import firebase from "../../utility/Firebase";
import * as Notifications from "expo-notifications";
import { LinearGradient } from "expo-linear-gradient";
import logoApp from "../../assets/logoApp.png";
import staySafe from "../../assets/staySafe.png";

//Notifications
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const dbh = firebase.firestore();

const { width, height } = Dimensions.get("screen");
export default function HomeScreen({ navigation }) {
  //************************* */
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [modalVisibal, setModalVisibal] = useState(false);
  const [openSettings, setOpenSettings] = useState(false);
  async function enterNotification() {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "❌❌ be careful ❌❌",
        body: "Il'ya qq1 porteur du COVID-19 proche de vous",
      },
      trigger: { seconds: 2 },
    });
  }
  async function leaveNotification() {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "✅✅ You are safe now ✅✅",
        body: "You are out of the infected zone ✅",
      },
      trigger: { seconds: 2 },
    });
  }
  // const regionMalade = [];
  const [regionMalade, setRegionMalade] = useState([]);
  let text = "Waiting..";
  let X = "";
  let Y = "";
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
    X = JSON.stringify(location.coords.latitude);
    Y = JSON.stringify(location.coords.longitude);
  }
  TaskManager.defineTask(
    "REGIONS_MALADE",
    ({ data: { eventType, region }, error }) => {
      // console.warn("aaaa");
      if (error) {
        // console.warn(error.message);
        return;
      }
      if (eventType === Location.GeofencingEventType.Enter) {
        // console.warn("You've entered region:", region);
        enterNotification();
      } else if (eventType === Location.GeofencingEventType.Exit) {
        // console.warn("You've left region:", region);
        leaveNotification();
      }
    }
  );
  // TaskManager.defineTask("background-location-task", ({ data, error }) => {
  //   if (error) {
  //     console.warn(error.message)
  //     return;
  //   }

  //   if (data) {
  //     const { locations } = data;
  //     console.log("Hello", locations);
  //   }
  // });

  useEffect(() => {
    dbh.collection("RegionMalade").onSnapshot((documentSnapshot) => {
      let data = [];
      let i = 1;
      documentSnapshot.forEach((doc) => {
        data.push({
          identifier: i.toString(),
          latitude: parseFloat(doc.data().location[0]),
          longitude: parseFloat(doc.data().location[1]),
          radius: 70,
          notifyOnEnter: true,
          notifyOnExit: true,
        });
        i++;
      });
      setRegionMalade(data);
      // console.warn("regionMalade: ", data);
    });
  }, []);
  // useEffect(() => {
  //   (async()=>{
  //     let { backStatus } = await Location.startLocationUpdatesAsync(
  //       "background-location-task",{
  //     accuracy: Location.Accuracy.High,
  //     timeInterval: 15000,
  //     showsBackgroundLocationIndicator: true,
  //     foregroundService: {
  //       notificationTitle: 'helloww bg geoloc',
  //       notificationBody: 'body teste nofif',
  //       notificationColor: '#EEE',
  //     }},
  //     );
  //     console.log("loc 1", backStatus);
  //   })
  // }, [])
  useEffect(() => {
    (async () => {
      try {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== "granted") {
          setErrorMsg("Permission to access location was denied");
          return;
        }

        let location = await Location.getCurrentPositionAsync({
          enableHighAccuracy: true,
        });
        setLocation(location);
      } catch (error) {
        let status = Location.getProviderStatusAsync();
        if (!status.locationServicesEnabled) {
          setModalVisibal(true);
        }
      }
    })();
  }, []);

  const scanRegionHandler = () => {
    if (regionMalade.length) {
      // console.warn("regionMalade 2: ", regionMalade);
      Location.startGeofencingAsync("REGIONS_MALADE", regionMalade)
        .then((response) => {
          // console.warn("bbb");
          // console.warn("resp: ", response);
          if (response) {
            // console.warn("Response from startGeoFencingAsync() = " + response);
          }
        })
        .catch((error) => {
          if (error) {
            // console.warn("Error from startGeoFencingAsync() = " + error);
          }
        });
    }
  };

  const settings = () => {
    if (Platform.OS == "ios") {
      Linking.openURL("app-settings:");
    } else {
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS
      );
    }
    setOpenSettings(false);
  };
  return (
    <View>
      <LinearGradient
        // Background Linear Gradient
        colors={["white", "#4096e9"]}
        style={{
          width: width,
          height: height,
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
        }}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1.3 }}
      />
      <View style={{ alignItems: "center", height: height * 0.25 }}>
        <Image
          source={logoApp}
          style={{
            width: width * 0.9,
            height: 250,
          }}
        />
      </View>
      <View
        style={{
          alignItems: "center",
          alignSelf: "center",
          height: height * 0.2,
          width: width * 0.8,
          paddingTop: "10%",
        }}
      >
        <Text style={{ textAlign: "center", fontSize: 16, paddingBottom: 5 }}>
          Thank you for your time !{" "}
        </Text>
        <Text style={{ textAlign: "center", fontSize: 16, paddingBottom: 5 }}>
          We hope that you always stay safe :){" "}
        </Text>
        <Text style={{ textAlign: "center", fontSize: 16, paddingBottom: 5 }}>
          If you get any Symptoms of the disease contact your doctor
        </Text>
      </View>
      <View
        style={{
          alignItems: "center",
          height: height * 0.35,
          paddingTop: "10%",
        }}
      >
        <Image
          source={staySafe}
          style={{
            width: width * 0.9,
            height: 250,
          }}
        />
      </View>
      <View
        style={{
          alignItems: "center",
          height: height * 0.2,
          justifyContent: "center",
        }}
      >
        <Button
          color="#3f97e9"
          style={{ width: "80%", borderRadius: 50 }}
          onPress={() => scanRegionHandler()}
        >
          Scan envirement !!!
        </Button>
        <TouchableOpacity
          style={{ paddingTop: 10 }}
          activeOpacity={0.7}
          onPress={() => navigation.reset({ routes: [{ name: "Questions" }] })}
        >
          <Text>Re-try the COVID-19 test again if you want !</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
