import React, { useState, useEffect } from "react";
import styles from "./style";
import { Alert, Modal, View, Linking } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Image, Dimensions, TouchableOpacity } from "react-native";
import SingleCardView from "react-native-simple-card";
import logoApp from "../../assets/logoApp.png";
// galio components
import { Button, Input, Text } from "galio-framework";
import * as Location from "expo-location";
import * as IntentLauncher from "expo-intent-launcher";
import "firebase/firestore";
import firebase from "../../utility/Firebase";
import { timestamp } from "../../utility/Firebase";
const dbh = firebase.firestore();
const { width, height } = Dimensions.get("screen");
export default function QuestionsScreen({ navigation }) {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [openSettings, setOpenSettings] = useState(false);
  const [cardStat1, setCardStat1] = useState(0);
  const [cardStat2, setCardStat2] = useState(0);
  const [cardStat3, setCardStat3] = useState(0);
  const [cardStat4, setCardStat4] = useState(0);
  const [cardStat5, setCardStat5] = useState(0);
  const [cardStat6, setCardStat6] = useState(0);
  // users count
  useEffect(() => {
    dbh.collection("users").onSnapshot((documentSnapshot) => {
      let i = 0;
      documentSnapshot.forEach(() => {
        i++;
      });
      setCardStat1(i);
    });
  }, []);
  // users infected count
  useEffect(() => {
    dbh.collection("RegionMalade").onSnapshot((documentSnapshot) => {
      let i = 0;
      documentSnapshot.forEach(() => {
        i++;
      });
      setCardStat2(i);
    });
  }, []);
  // total ansers
  useEffect(() => {
    dbh.collection("Resultat").onSnapshot((documentSnapshot) => {
      let i = 0;
      let j = 0;
      documentSnapshot.forEach((doc) => {
        i++;
        console.warn("doc.data().malade", doc.data().reponse.malade);
        if (doc.data().reponse.malade === true) {
          console.warn("user malade");
          if (doc.data().reponse.dateRecover > Date.now()) {
            console.warn("user malade counter ++");
            j++;
          }
        }
      });
      setCardStat3(i);
      setCardStat5(j);
    });
  }, []);
  // taux de recouvrement
  useEffect(() => {
    if (cardStat3 != 0 || cardStat2 != 0) {
      setCardStat4(((cardStat5 / cardStat3) * 100).toFixed(2));
    } else {
      setCardStat4(0);
    }
  }, [cardStat1, cardStat3, cardStat5]);
  //persone malede /jour
  useEffect(() => {
    dbh.collection("Resultat").onSnapshot((documentSnapshot) => {
      let i = 0;
      documentSnapshot.forEach((doc) => {
        if (doc.data().reponse.malade === true) {
          if (
            Date.now() + 1209600000 > doc.data().reponse.dateRecover &&
            Date.now() + 1123200000 < doc.data().reponse.dateRecover
          )
            i++;
        }
      });
      setCardStat6(i);
    });
  }, []);

  useEffect(() => {
    (async () => {
      try {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== "granted") {
          setErrorMsg("Permission to access location was denied");
          return;
        }

        let location = await Location.getCurrentPositionAsync({
          enableHighAccuracy: true,
        });
        setLocation(location);
      } catch (error) {
        let status = Location.getProviderStatusAsync();
        if (!status.locationServicesEnabled) {
          setModalVisibal(true);
        }
      }
    })();
  }, []);

  let text = "Waiting..";
  let X = "";
  let Y = "";
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
    X = JSON.stringify(location.coords.latitude);
    Y = JSON.stringify(location.coords.longitude);
  }
  const settings = () => {
    if (Platform.OS == "ios") {
      Linking.openURL("app-settings:");
    } else {
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS
      );
    }
    setOpenSettings(false);
  };
  //Modals
  const [modalSain, setModalSain] = useState(false);
  const [modalMalade, setModalMalade] = useState(false);
  const [modalChance, setModalChance] = useState(false);
  //Questions View
  const [q1, setQ1] = useState(1);
  const [q2, setQ2] = useState(0);
  const [q3, setQ3] = useState(0);
  const [q4, setQ4] = useState(0);
  const [q5, setQ5] = useState(0);
  const [q6, setQ6] = useState(0);
  const [q7, setQ7] = useState(0);
  const [q8, setQ8] = useState(0);
  const [q9, setQ9] = useState(0);
  const [q10, setQ10] = useState(0);
  const [q11, setQ11] = useState(0);
  const [q12, setQ12] = useState(0);
  const [q13, setQ13] = useState(0);
  const [q14, setQ14] = useState(0);
  const [q15, setQ15] = useState(0);
  const [q16, setQ16] = useState(0);
  const [q17, setQ17] = useState(0);
  const [sum, setSum] = useState(0);
  const [sumTottal, setSumTottal] = useState(0);
  const [resultat, setResultat] = useState(0);

  //Repences value
  const [clickedOui, setclickedOui] = useState(false);
  const [clickedNon, setclickedNon] = useState(false);
  const [r1, setR1] = useState("");
  const [r2, setR2] = useState("");
  const [r3, setR3] = useState("");
  const [r4, setR4] = useState("");
  const [r5, setR5] = useState("");
  const [r6, setR6] = useState("");
  const [r7, setR7] = useState("");
  const [r8, setR8] = useState("");
  const [r9, setR9] = useState("");
  const [r10, setR10] = useState("");
  const [r11, setR11] = useState("");
  const [r12, setR12] = useState("");
  const [r13, setR13] = useState("");
  const [r14, setR14] = useState("");
  const [r15, setR15] = useState("");
  const [r16, setR16] = useState("");
  const [r17, setR17] = useState("");
  const [malade, setMalade] = useState(false);
  const [reps, setreps] = useState({
    r1: r1,
    r2: r2,
    r3: r3,
    r4: r4,
    r5: r5,
    r6: r6,
    age: r7,
    r8: r8,
    r9: r9,
    r10: r10,
    r11: r11,
    r12: r12,
    r13: r13,
    r14: r14,
    r15: r15,
    r16: r16,
    r17: r17,
    malade: malade,
    creationDate: timestamp(),
    dateRecover: Date.now() + 1209600000,
  });
  const [localisation, setLocalisation] = useState({
    X: "",
    Y: "",
  });

  const question1 = () => {
    setQ1(0);
    setQ2(1);
    setLocalisation([X, Y]);
    setreps({ ...reps, r1: r1 });
  };
  const question2 = () => {
    setQ2(0);
    setQ3(1);
    setreps({ ...reps, r2: r2 });
  };
  const question3 = () => {
    setQ3(0);
    setQ4(1);
    setreps({ ...reps, r3: r3 });
  };
  const question4 = () => {
    setQ4(0);
    setQ5(1);
    setreps({ ...reps, r4: r4 });
  };
  const question5 = () => {
    setQ5(0);
    setQ6(1);
    setreps({ ...reps, r5: r5 });
  };
  const question6 = () => {
    setQ6(0);
    setQ7(1);
    setreps({ ...reps, r6: r6 });
  };
  const question7 = () => {
    setQ7(0);
    setQ8(1);
    setreps({ ...reps, age: r7 });
  };
  const question8 = () => {
    setQ8(0);
    setQ9(1);
    setreps({ ...reps, r8: r8 });
  };
  const question9 = () => {
    setQ9(0);
    setQ10(1);
    setreps({ ...reps, r9: r9 });
  };
  const question10 = () => {
    setQ10(0);
    setQ11(1);
    setreps({ ...reps, r10: r10 });
  };
  const question11 = () => {
    setQ11(0);
    setQ12(1);
    setreps({ ...reps, r11: r11 });
  };
  const question12 = () => {
    setQ12(0);
    setQ13(1);
    setreps({ ...reps, r12: r12 });
  };
  const question13 = () => {
    setQ13(0);
    setQ14(1);
    setreps({ ...reps, r13: r13 });
  };
  const question14 = () => {
    setQ14(0);
    setQ15(1);
    setreps({ ...reps, r14: r14 });
  };
  const question15 = () => {
    setQ15(0);
    setQ16(1);
    setreps({ ...reps, r15: r15 });
  };
  const question16 = () => {
    setQ16(0);
    setQ17(1);
    setreps({ ...reps, r16: r16 });
  };
  const question17 = () => {
    setQ17(0);
    setResultat(1);
    setreps({ ...reps, r17: r17 });
    if (
      parseInt(r1) &&
      parseInt(r2) &&
      parseInt(r3) &&
      parseInt(r4) &&
      parseInt(r5) &&
      parseInt(r6)
    ) {
      setreps({ ...reps, malade: true });
    }
    setSum(
      parseInt(r1) +
        parseInt(r2) +
        parseInt(r3) +
        parseInt(r4) +
        parseInt(r5) +
        parseInt(r6)
    );
    setSumTottal(
      parseInt(r1) +
        parseInt(r2) +
        parseInt(r3) +
        parseInt(r4) +
        parseInt(r5) +
        parseInt(r6) +
        parseInt(r8) +
        parseInt(r9) +
        parseInt(r10) +
        parseInt(r11) +
        parseInt(r12) +
        parseInt(r13) +
        parseInt(r14) +
        parseInt(r15) +
        parseInt(r16) +
        parseInt(r17)
    );
  };

  const resultess = () => {
    // console.warn("sum:" + sum);
    // console.warn("sumTottal:" + sumTottal);
    if (
      parseInt(r1) &&
      parseInt(r2) &&
      parseInt(r3) &&
      parseInt(r4) &&
      parseInt(r5) &&
      parseInt(r6)
    ) {
      setModalMalade(true);
      dbh
        .collection("RegionMalade")
        .doc()
        .set({
          location: localisation,
        })
        .then(() => {});
    } else if ((sum >= 3 && sum < 6) || (sumTottal >= 8 && sumTottal < 13)) {
      setModalChance(true);
    } else {
      setModalSain(true);
    }

    // setModalSain(true);
    dbh
      .collection("Resultat")
      .doc()
      .set({
        user: firebase.auth().currentUser.uid,
        reponse: reps,
      })
      .then(() => {});
    // console.warn(reps);
  };
  const modalSainHandler = () => {
    setModalSain(!modalSain);
    navigation.navigate("Home");
  };
  const modalChanceHandler = () => {
    setModalChance(!modalChance);
    navigation.navigate("Home");
  };
  const modalMaladeHandler = () => {
    setModalMalade(!modalMalade);
    navigation.navigate("Home");
  };
  return (
    <View style={styles.container}>
      <LinearGradient
        // Background Linear Gradient
        colors={["white", "#4096e9"]}
        style={{
          width: width,
          height: height,
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
        }}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1.3 }}
      />
      <View style={{ alignItems: "center", height: height * 0.25 }}>
        <Image
          source={logoApp}
          style={{
            width: width * 0.9,
            height: 250,
          }}
        />
      </View>
      {/* <Text style={{ fontSize: 16 }}>Latitude: {X}</Text>
      <Text style={{ fontSize: 16 }}>Longitude: {Y}</Text> */}
      {/* Modal Sain */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalSain}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalSain(!modalSain);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                width: "100%",
                height: "80%",
                alignItems: "center",
              }}
            >
              <Text style={styles.modalText}>Votre resultat!</Text>
              <Text
                style={{
                  color: "green",
                  fontSize: 20,
                  fontWeight: "bold",
                  textAlign: "center",
                }}
              >
                {" "}
                Vous êtes en bonne santé !! Protégez vous{" "}
              </Text>
            </View>
            <View
              style={{
                justifyContent: "flex-end",
                width: "100%",
                height: "20%",
                alignItems: "center",
              }}
            >
              <Button
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  modalSainHandler();
                }}
              >
                <Text style={styles.textStyle}>Hide Modal</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>

      {/* Modal Malade */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalMalade}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalMalade(!modalMalade);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                width: "100%",
                height: "80%",
                alignItems: "center",
              }}
            >
              <Text style={styles.modalText}>Votre resultat!</Text>
              <Text
                style={{
                  color: "red",
                  fontSize: 20,
                  fontWeight: "bold",
                  textAlign: "center",
                }}
              >
                {" "}
                Vous êtes infectés par le Covid-19 ! Restez chez vous{" "}
              </Text>
            </View>
            <View
              style={{
                justifyContent: "flex-end",
                width: "100%",
                height: "20%",
                alignItems: "center",
              }}
            >
              <Button
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  modalMaladeHandler();
                }}
              >
                <Text style={styles.textStyle}>Hide Modal</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>

      {/* Modal Chance */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalChance}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalChance(!modalChance);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                width: "100%",
                height: "80%",
                alignItems: "center",
              }}
            >
              <Text style={styles.modalText}>Votre resultat!</Text>
              <Text
                style={{
                  color: "orange",
                  fontSize: 20,
                  fontWeight: "bold",
                  textAlign: "center",
                }}
              >
                {" "}
                Vous êtes 80% infecté par le COVID-19!, Restez chez vous{" "}
              </Text>
            </View>
            <View
              style={{
                justifyContent: "flex-end",
                width: "100%",
                height: "20%",
                alignItems: "center",
              }}
            >
              <Button
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  modalChanceHandler();
                }}
              >
                <Text style={styles.textStyle}>Hide Modal</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>

      {q1 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous des difficultés à respirer ou essoufflement ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR1(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR1(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR1(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR1(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question1();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q2 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous du sensation d’oppression ou douleur au niveau de la
            poitrine ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR2(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR2(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR2(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR2(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question2();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q3 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous du perte d’élocution ou de motricité ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR3(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR3(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR3(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR3(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question3();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q4 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous du perte de l’odorat ou du goût ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR4(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR4(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR4(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR4(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question4();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q5 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous du fièvre ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR5(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR5(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR5(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR5(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question5();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q6 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez-vous du fatigue ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR6(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR6(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR6(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR6(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question6();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q7 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Veuillez préciser votre poids (kg)?
          </Text>

          <View style={{ alignItems: "center" }}>
            <Input
              placeholder="regular"
              type="numeric"
              onChangeText={(age) => setR7(age)}
              style={{ width: "80%", marginBottom: 20 }}
            />
            <Button
              color="#3f97e9"
              style={{
                borderRadius: 50,
                marginBottom: 0,
              }}
              onPress={() => question7()}
            >
              Suivant
            </Button>
          </View>
        </View>
      )}
      {q8 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16, alignSelf: "center" }}>
            Avez vous été atteint de la COVID-19
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR8(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR8(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR8(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR8(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question8();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}

      {q9 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous atteint du diabète ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR9(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR9(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR9(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR9(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question9();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}

      {q10 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous atteint d'hypertension artérielle ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR10(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR10(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR10(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR10(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question10();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q11 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous atteint d'une maladie rénale chromique ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR11(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR11(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR11(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR11(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question11();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q12 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous atteint de l'insuffisance cardiaque ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR12(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR12(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR12(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR12(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question12();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q13 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Avez-vous suivi des traitement anti-cancers ou immunosuppresseurs ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR13(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR13(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR13(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR13(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question13();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q14 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Avez-vous été atteint d'une maladie respiratoire chronique ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR14(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR14(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR14(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR14(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question14();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q15 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Avez-vous subi une greffe d'organe ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR15(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR15(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR15(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR15(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question15();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q16 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous en contact avec une personne testée positive ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR16(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR16(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR16(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR16(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question16();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {q17 == 1 && (
        <View style={styles.card}>
          <Text style={{ margin: "10%", fontSize: 16 }}>
            Etes-vous en contact avec une personne dont son entourage a été (ou
            est) testé positif au COVID-19 ?
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            {clickedOui ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR17(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR17(1);
                  setclickedOui(true);
                  setclickedNon(false);
                }}
              >
                Oui
              </Button>
            )}
            {clickedNon ? (
              <Button
                color="green"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR17(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            ) : (
              <Button
                color="#3f97e9"
                style={{ width: "30%", borderRadius: 50, marginBottom: 0 }}
                onPress={() => {
                  setR17(0);
                  setclickedOui(false);
                  setclickedNon(true);
                }}
              >
                Non
              </Button>
            )}
          </View>
          <View style={{ alignItems: "center", marginTop: 40 }}>
            <Button
              color="#3f97e9"
              style={{
                width: "80%",
                borderRadius: 50,
                marginTop: 20,
              }}
              onPress={() => {
                if (clickedOui || clickedNon) {
                  setclickedOui(false);
                  setclickedNon(false);
                  question17();
                }
              }}
            >
              Next
            </Button>
          </View>
        </View>
      )}
      {resultat == 1 && (
        <View>
          <View
            style={{
              height: height * 0.6,
              justifyContent: "flex-start",
            }}
          >
            <View
              style={{ flexDirection: "row", justifyContent: "space-around" }}
            >
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={0}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>Users:</Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat1}
                </Text>
              </SingleCardView>
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={0}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>Total ansers:</Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat3}
                </Text>
              </SingleCardView>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-around" }}
            >
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={170}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>
                  Users infected:
                </Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat2}
                </Text>
              </SingleCardView>
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={170}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>
                  Users Recovered:
                </Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat5}
                </Text>
              </SingleCardView>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-around" }}
            >
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={340}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>
                  Personnes atteintes /jour:
                </Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat6}
                </Text>
              </SingleCardView>
              <SingleCardView
                elevation={1}
                shadowColor="rgb(50,50,50)"
                // shadowOpacity={1}
                height={height * 0.17}
                width={width * 0.42}
                marginTop={340}
                style={styles.statCard}
              >
                <Text style={{ padding: 10, fontSize: 20 }}>
                  Taux de recouvrement:
                </Text>
                <Text
                  style={{
                    padding: 10,
                    fontSize: 24,
                    alignSelf: "center",
                    fontWeight: "bold",
                  }}
                >
                  {cardStat4}%
                </Text>
              </SingleCardView>
            </View>
          </View>
          <View
            style={{
              alignItems: "center",
              height: height * 0.15,
              justifyContent: "flex-end",
            }}
          >
            <Button
              size="large"
              color="primary"
              round
              uppercase
              style={{ marginBottom: 15 }}
              onPress={() => resultess()}
            >
              Check your results
            </Button>
            <TouchableOpacity
              style={{ marginBottom: 10 }}
              activeOpacity={0.7}
              onPress={() =>
                navigation.reset({ routes: [{ name: "Questions" }] })
              }
            >
              <Text>Re-try the COVID-19 test again if you want !</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
}
