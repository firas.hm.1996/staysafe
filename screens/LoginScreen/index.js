import React, { useState } from "react";
import styles from "./style";
import {
  Alert,
  Dimensions,
  Image,
  Modal,
  View,
  TouchableOpacity,
} from "react-native";
import { Block, Button, Input, Text } from "galio-framework";
import { LinearGradient } from "expo-linear-gradient";
import "firebase/firestore";
import firebase from "../../utility/Firebase";
const dbh = firebase.firestore();
const { height, width } = Dimensions.get("window");
const logoApp = require("../../assets/logoApp.png");

export default function LoginScreen({ navigation }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [erreur, setErreur] = useState("");
  const login = () => {
    if (email != "" || password != "") {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          navigation.reset({ routes: [{ name: "Questions" }] });
        })
        .catch((error) => {
          // alert(error.message);
          setErreur(error.message);
          setModalVisible(!modalVisible);
        });
    } else {
      Alert.alert("Le champ E-mail ou Password est vide.");
    }
  };
  return (
    <Block safe flex>
      <View style={styles.container} enabled>
        <LinearGradient
          // Background Linear Gradient
          colors={["white", "#4096e9"]}
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            width: width,
            height: height,
          }}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1.3 }}
        />
        <Block
          flex
          center
          style={{
            width: "100%",
          }}
        >
          <Image
            source={logoApp}
            style={{ width: 300, height: 250, resizeMode: "contain" }}
          />
        </Block>
        <Block flex={2} center space="evenly">
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View
                  style={{
                    width: "100%",
                    height: "80%",
                    alignItems: "center",
                  }}
                >
                  <Text style={styles.modalText}>ops !!</Text>
                  <Text style={styles.validText}> {erreur} </Text>
                </View>
                <View
                  style={{
                    justifyContent: "flex-end",
                    width: "100%",
                    height: "20%",
                    alignItems: "center",
                  }}
                >
                  <Button
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => {
                      setModalVisible(!modalVisible);
                    }}
                  >
                    <Text style={styles.textStyle}>Try again !</Text>
                  </Button>
                </View>
              </View>
            </View>
          </Modal>
          <Block flex={2}>
            <Input
              placeholder="E-mail"
              rounded
              type="email-address"
              placeholderTextColor="#999"
              onChangeText={(email) => setEmail(email)}
            />
            <Input
              rounded
              password
              viewPass={true}
              placeholder="Password"
              placeholderTextColor="#999"
              style={{ width: width * 0.9 }}
              onChangeText={(password) => setPassword(password)}
            />
            <Text
              onPress={() => Alert.alert("Not implemented")}
              color="#5373b8"
              style={{
                alignSelf: "flex-end",
              }}
            >
              Forgot your password?
            </Text>
          </Block>

          <Button round color="#5373b8" onPress={() => login()}>
            Sign in
          </Button>

          <TouchableOpacity
            style={{ marginBottom: 10 }}
            activeOpacity={0.7}
            onPress={() => navigation.navigate("Register")}
          >
            <Text>Don't have an account? Sign Up</Text>
          </TouchableOpacity>
        </Block>
      </View>
    </Block>
  );
}
