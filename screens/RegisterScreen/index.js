import React, { useState } from "react";
import styles from "./style";
import { Dimensions, Image, View } from "react-native";
import { Block, Button, Input } from "galio-framework";
import "firebase/firestore";
import firebase from "../../utility/Firebase";
import { LinearGradient } from "expo-linear-gradient";
const { height, width } = Dimensions.get("window");
const logoApp = require("../../assets/logoApp.png");
const dbh = firebase.firestore();
export default function RegisterScreen({ navigation }) {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  return (
    <Block safe flex>
      <View style={styles.container} enabled>
        <LinearGradient
          // Background Linear Gradient
          colors={["white", "#4096e9"]}
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            width: width,
            height: height,
          }}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1.3 }}
        />
        <Block
          flex
          center
          style={{
            width: "100%",
          }}
        >
          <Image
            source={logoApp}
            style={{ width: 300, height: 250, resizeMode: "contain" }}
          />
        </Block>
        <Block flex={2} center space="evenly">
          <Block flex={2}>
            <Input
              placeholder="E-mail"
              type="email-address"
              rounded
              placeholderTextColor="#999"
              onChangeText={(email) => setEmail(email)}
            />
            <Input
              rounded
              password
              viewPass={true}
              placeholder="Password"
              placeholderTextColor="#999"
              style={{ width: width * 0.9 }}
              onChangeText={(password) => setPassword(password)}
            />
            <Input
              rounded
              type="numeric"
              placeholder="Phone Number"
              placeholderTextColor="#999"
              style={{ width: width * 0.9 }}
              onChangeText={(phone) => setPhone(phone)}
            />
          </Block>

          <Button
            round
            color="#5373b8"
            style={{ marginBottom: "10%" }}
            onPress={() => register(email, password, phone, navigation)}
          >
            Sign Up
          </Button>
        </Block>
      </View>
    </Block>
  );
}

const register = (email, password, phone, navigation) => {
  if (email != "" && phone != "" && password != "")
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        dbh
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .set({
            email,
            password,
            phone,
          })
          .then(function () {
            firebase.auth().signInWithEmailAndPassword(email, password);
            navigation.reset({ routes: [{ name: "Questions" }] });
          });
      });
};
