import * as firebase from "firebase";

// Optionally import the services that you want to use
//import "firebase/auth";
//import "firebase/database";
//import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBQzZlmIz4tJ-2xyDdyd2mt2FMXKaXA3pU",
  authDomain: "staysafe-515ad.firebaseapp.com",
  projectId: "staysafe-515ad",
  storageBucket: "staysafe-515ad.appspot.com",
  messagingSenderId: "485892124725",
  appId: "1:485892124725:web:5d730213c0a0770b6cc17c",
  measurementId: "G-0Y58DLHL6X",
};
export const timestamp = firebase.firestore.FieldValue.serverTimestamp;
export default firebase.initializeApp(firebaseConfig);
